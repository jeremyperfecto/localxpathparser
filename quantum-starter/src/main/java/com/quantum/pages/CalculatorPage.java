/**
 * 
 */
package com.quantum.pages;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.LocalParseUtils;
import com.quantum.utils.PageValidator;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * @author chirag.jayswal
 *
 */
@QAFTestStepProvider
public class CalculatorPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "input.box")
	private QAFExtendedWebElement inputBox;

	@FindBy(locator = "btn.plus")
	private QAFExtendedWebElement btnPlus;

	@FindBy(locator = "btn.multiplication")
	private QAFExtendedWebElement btnMultiplication;

	@FindBy(locator = "btn.division")
	private QAFExtendedWebElement btnDivision;

	@FindBy(locator = "btn.minus")
	private QAFExtendedWebElement btnMinus;

	@FindBy(locator = "btn.equal")
	private QAFExtendedWebElement btnEqual;

	@FindBy(locator = "btn.clear")
	private QAFExtendedWebElement btnClear;


	
	public CalculatorPage()
	{
		DeviceUtils.switchToContext("NATIVE_APP");
	}
	
	public void startApp(String name)
	{
		DeviceUtils.startApp(name, "name");
		btnClear.waitForVisible(1000);
	}
	

	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
