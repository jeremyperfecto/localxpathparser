package com.quantum.tests.calculator;

import java.io.IOException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.qmetry.qaf.automation.core.TestBaseProvider;
import com.qmetry.qaf.automation.data.MetaData;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.quantum.pages.CalculatorPage;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.DriverUtils;
import com.quantum.utils.LocalParseUtils;
import com.quantum.utils.PageValidator;

public class ParsingTests extends WebDriverTestCase {

	@Test(groups = { "regression" })
	public void TestParsing() throws Exception {

		CalculatorPage cp = new CalculatorPage();
		cp.startApp("Calculator");

		Document doc = LocalParseUtils.getXML();
		
		//print all labels
		NodeList nodes2 = LocalParseUtils.getXPathList(doc, "//XCUIElementTypeButton[@label]");

		for (int i = 0; i < nodes2.getLength(); i++) {

			System.out.println(LocalParseUtils.getXPathAttribute(nodes2.item(i), "label"));
			
		}
		
		//print all names
		NodeList nodes3 = LocalParseUtils.getXPathList(doc, "//XCUIElementTypeButton[@name]");

		for (int i = 0; i < nodes3.getLength(); i++) {

			System.out.println(LocalParseUtils.getXPathAttribute(nodes3.item(i), "name"));
			
		}
		
		PageValidator pv = new PageValidator();
		//validate all names
		pv.validatePageContent(pv.getValiationPath("validators.loc"), "validator", "//XCUIElementTypeButton[@name='%s']");
		
		//validate all xpaths
		pv.validatePageContent(pv.getValiationPath("validators.loc"), "validator", "//XCUIElementTypeButton[@label='%s']");

		
	}
}
