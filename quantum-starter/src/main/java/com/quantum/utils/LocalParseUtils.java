package com.quantum.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.text.StringEscapeUtils;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.common.base.Verify;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;

public class LocalParseUtils extends WebDriverTestBase {
	
	public static Document getXML() throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder();
		Document doc = builder.parse(new ByteArrayInputStream(DriverUtils.getDriver().getPageSource().getBytes()));
		return doc;
	}
	
	public static Node getXpathNode(Document doc, String XpathString) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException
	{
		NodeList result = getXPathList(doc, XpathString);

		if (result.item(0) == null) {
			throw new XPathExpressionException("Xpath not found");
		} else {
			return result.item(0);
		}
	}
	
	public static Node getXpathNode(String XpathString) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException
	{
		NodeList result = getXPathList(getXML(), XpathString);

		if (result.item(0) == null) {
			throw new XPathExpressionException("Xpath not found");
		} else {
			return result.item(0);
		}
	}
	
	public static String getXPathValue(String XpathString)
			throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

		NodeList result = getXPathList(getXML(), XpathString);

		if (result.item(0) == null) {
			throw new XPathExpressionException("Xpath not found");
		} else {
			return result.item(0).getTextContent();
		}
	}

	public static String getXPathValue(Document doc, String XpathString)
			throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

		NodeList result = getXPathList(doc, XpathString);

		if (result.item(0) == null) {
			throw new XPathExpressionException("Xpath not found");
		} else {
			return result.item(0).getTextContent();
		}
	}

	public static String getXPathValue(Node node)
			throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {


		if (node == null) {
			throw new XPathExpressionException("Xpath not found");
		} else {
			return node.getTextContent();
		}
	}
	
	public static String getXPathAttribute(String attribute, String XpathString)
			throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

		NodeList result = getXPathList(getXML(), XpathString);

		if (result.item(0) == null) {
			throw new XPathExpressionException("Xpath not found");
		} else {
			return result.item(0).getAttributes().getNamedItem(attribute).getTextContent();
		}
	}

	public static String getXPathAttribute(Document doc, String attribute, String XpathString)
			throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

		NodeList result = getXPathList(doc, XpathString);

		if (result.item(0) == null) {
			throw new XPathExpressionException("Xpath not found");
		} else {
			return result.item(0).getAttributes().getNamedItem(attribute).getTextContent();
		}
	}

	public static String getXPathAttribute(Node node, String attribute)
			throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

		if (node == null) {
			throw new XPathExpressionException("Xpath not found");
		} else {
			return node.getAttributes().getNamedItem(attribute).getTextContent();
		}
	}

	public static NodeList getXPathList(Document doc, String XpathString)
			throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

		try
		{
		XPath xpath = XPathFactory.newInstance().newXPath();
		XPathExpression expr = xpath.compile(XpathString);
		NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);

		return (NodeList) nodes;
		}
		catch(Exception ex)
		{
			throw new XPathExpressionException(" invalid xpath syntax or xpath not found: " +ex.toString());
		}
	}

}
