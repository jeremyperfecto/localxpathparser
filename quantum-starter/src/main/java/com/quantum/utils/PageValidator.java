/**
 * 
 */
package com.quantum.utils;

import static org.testng.Assert.assertNotNull;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.google.common.base.Verify;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.StringMatcher;
import com.qmetry.qaf.automation.util.StringUtil;
import com.quantum.utils.*;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

@QAFTestStepProvider
public class PageValidator extends WebDriverTestBase {

	public String getValiationPath(String file) {
		return ConfigurationManager.getBundle().getString("env.resources") + "/" + file;
	}

	private void internalValidatePageContent(Document doc, String file, String object, String xpath)
			throws IOException {

		Properties props = new Properties();
		FileInputStream in = new FileInputStream(file);
		props.load(new InputStreamReader(in, Charset.forName("UTF-8")));
		in.close();

		Set<String> keys = props.stringPropertyNames();
		for (String key : keys) {
			if (key.contains(object)) {
				try {
					Node n = LocalParseUtils.getXpathNode(doc, String.format(xpath, props.getProperty(key).trim()));
					verify(String.format(xpath, props.getProperty(key).trim()), n);
				} catch (Exception ex) {

				}

			}

		}

	}

	public void validatePageContent(String file, String object, String xpath) throws Exception {

		Document doc = LocalParseUtils.getXML();
		internalValidatePageContent(doc, file, object, xpath);

	}

	public void validatePageContent(Document doc, String file, String object, String xpath) throws Exception {

		internalValidatePageContent(doc, file, object, xpath);
	}

	public void verify(String s, Node n) {
		try {

			ConsoleUtils.logInfoBlocks(s + " Assert");
			Verify.verifyNotNull(n);
			ReportUtils.logVerify(s + " Assert", true);
		} catch (Exception ex) {
			ReportUtils.logVerify(s + " Assert", false);
		}
	}
}
